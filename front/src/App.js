import './App.css';
import Login from './pages/login';
import Register from "./pages/register";
import Home from "./pages/home";
import Chat from "./pages/chat";
import { Routes, Route } from "react-router-dom";
import ThemeProvider, { ThemeContext } from "./Contexts/UserContext";

const App =() => {
  	return (
	<ThemeProvider>
		<ThemeContext.Consumer>
			{({ userName, setUsername }) => (
				<Routes>
        			<Route path="/" element={<Home />} />
        			<Route path="Login" element={<Login />} />
					<Route path="register" element={<Register />} />
					<Route path='chat' element={<Chat value={userName} />} />
    			</Routes>	
				)}
		</ThemeContext.Consumer>	
	</ThemeProvider>
	)
}


export default App;