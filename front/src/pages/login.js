import { useCallback, useState, createContext, useContext } from "react";
import { signInWithEmailAndPassword } from "firebase/auth";
import { auth, db } from "../firebase";
import { useNavigate } from "react-router-dom";
import { setDoc } from "firebase/firestore";
import { ThemeContext } from "../Contexts/UserContext";


export default function Login(){
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [error, setError] = useState("");
    const navigate = useNavigate(); 
    const [pseudo, setPseudo] = useState("");
	const handlePseudo = useCallback((e) => {
		setPseudo(e.target.value);
	}, [setPseudo]);


    const {setUserName} = useContext(ThemeContext);
    const handleEmailChange = useCallback((e) => {
        setEmail(e.target.value);
    }, [setEmail]);

    const handlePasswordChange = useCallback((e) => {
        setPassword(e.target.value);
    }, [setPassword]);

    const handleSubmit = useCallback((e) => {
        e.preventDefault();
        setUserName(pseudo);
        signInWithEmailAndPassword(auth, email, password)
            .then((userCredential) => {
    // Signed in 
        const user = userCredential.user;
        localStorage.setItem("access_token", user.accessToken);
        console.log(userCredential.user);
        navigate("/chat");
        setDoc(db, "users", user.uid, {
            pseudo: pseudo,
            email: user.email,
            photoURL: user.photoURL,
            displayName: user.displayName,
            createdAt: user.createdAt
        });
        })
        .catch((error) => {
        setError(error.message);
     });
    }, [email, password, navigate, pseudo]);
    return (
        <div>
            <h1>Login</h1>
            <form onSubmit={handleSubmit}>
                <label htmlFor="email">Email</label>
                <input type="email" id="email" onChange={handleEmailChange} />
                <label htmlFor="password">Password</label>
                <input type="password" id="password" onChange={handlePasswordChange} />
                <label htmlFor="pseudo">Pseudo</label>
				<input type="pseudo" id="pseudo" onChange={handlePseudo} />
                <button type="submit">Login</button>
                {error && <p>{error}</p>}
            </form>
        </div>
    );
}
