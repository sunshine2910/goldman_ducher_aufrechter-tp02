import React, { useState, useEffect, useRef, useContext } from "react";
import { db } from "../firebase";
import { addDoc, doc, getDoc } from "firebase/firestore";
import { collection, query, getDocs } from "firebase/firestore";

const URL = 'ws://127.0.0.1:5000/chat';

const Chat =(props) => {
	const user = props.value;
	const [data	, setData] = useState([]);
  	const [message, setMessage] = useState("");
  	const [messages, setMessages] = useState([]);
	const ws = useRef()
	

	const handleHistory = () => {

const docRef = doc(db, "users", user, "messages");
const docSnap = getDoc(docRef);

if (docSnap.exists()) {
  console.log("Document data:", docSnap.data());
} else {
  // doc.data() will be undefined in this case
  console.log("No such document!");
}
}



  	const submitMessage = (usr, msg, sentAt) => {
  		const message = { user: usr, message: msg, date : sentAt };
  		ws.current.send(JSON.stringify(message));
		  try {
			const user = props.value;
			const docRef = addDoc(collection(db, "users", user, "messages"), {
				messages:{msg}
			  });
			  console.log("Document written with ID: ", docRef.id);
		  } catch (e) {
			console.error("Error adding document: ", e);
		  }
  	}

	useEffect(()=>{
		ws.current = new WebSocket(URL + '?access_token=' + (localStorage.getItem('access_token') || ''));
		ws.current.onopen = () => {
			console.log('WebSocket Connected');
		  }
			ws.current.onclose = () => {
			  console.log('WebSocket Disconnected');
			}
	  return()=>{
		  if(ws.current){
			ws.current.close()
		  }
	  }
	},[])

  	useEffect(() => {
		  if(ws.current){
			  ws.current.onmessage = (e) => {
				const message = JSON.parse(e.data);
				setMessages([message.payload, ...messages]);
			  }
		  }
          // eslint-disable-next-line
  	}, [ws.current, messages]);
  	return (
	    <div>
	        {props.value}
	        <ul>
	          {messages.reverse().map((message, index) =>
	            <li key={index}>
					<p>{message.sentAt}</p>
	              <b>{message.user}</b>: <em>{message.message}</em>
	            </li>
	          )}
	        </ul>

	        <form
	          action=""
	          onSubmit={e => {
	            e.preventDefault();
	            submitMessage(user, message);
	            setMessage("");
	          }}
	        >
	          <input
	            type="text"
	            placeholder={'Type a message ...'}
	            value={message}
	            onChange={e => setMessage(e.target.value)}
	          />
	          <input type="submit" value={'Send'} />
	        </form>
			<input type="submit" onClick={handleHistory} value="history" />
			{data}
	    </div>
  	)
}

export default Chat;