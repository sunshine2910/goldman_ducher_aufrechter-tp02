import admin from "firebase-admin";
import { getFirestore, Timestamp, FieldValue } from 'firebase-admin/firestore'
import serviceAccount from "../firebase.json" assert {type: "json"};


admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

export const db = getFirestore();
export {admin}
export const auth = admin.auth();